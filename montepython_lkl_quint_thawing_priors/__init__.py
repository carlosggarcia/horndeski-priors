#!/usr/bin/python
from montepython.likelihood_class import Likelihood
import numpy as np



###########################
# Define things
###########################

def exp_fit(X, c0, c1, c2):
    return c0 * np.exp(-np.abs(X  / c2)**c1)

def exp_fit_sum(X, c0, c1, c2, c3, c4, c5):
    return exp_fit(X, c0, c1, c2) + exp_fit(X, c3, c4, c5)

def gauss_dist(X, sigma):
    return np.exp(-0.5 * np.abs(X / sigma) ** 2) / (sigma * np.sqrt(2 * np.pi))

def poly(X, c):
    return np.sum([ci * X ** i for i, ci in enumerate(c)], axis=0)

def get_imodel(model):
    d = {'axion': 0,
        'monomial': 1,
         'eft': 2,
         'modulus':3}
    return d[model]

class quint_thawing_priors(Likelihood):
    # initialisation of the class is done within the parent Likelihood_prior. For
    # this case, it does not differ, actually, from the __init__ method in
    # Likelihood class.

    def Pw0(self, w0, imodel):
        if w0 < -1:
            return 0

        return exp_fit_sum(w0 + 1, *self.Pw0_c[imodel])

    def sigma_w0(self, w0, imodel):
        scoeffs = self.Pwaw0_c[imodel]
        return poly(w0, scoeffs)

    def Pwa_w0(self, wa, w0, imodel):
        sigma = self.sigma_w0(w0, imodel)
        mean_wa = poly(w0, self.mean_wa_c[imodel])
        if wa > 0:  # This might need to be changed
            return 0
        return gauss_dist(wa - mean_wa, sigma)

    def Pw0wa(self, w0, wa, imodel):
        return self.Pw0(w0, imodel) * self.Pwa_w0(wa, w0, imodel)

    def loglkl(self, cosmo, data):
        self.Pw0_c = np.array(self.Pw0_c)
        self.mean_wa_c = np.array(self.mean_wa_c)
        self.Pwaw0_c = np.array(self.Pwaw0_c)

        w0 = cosmo.pars['w0_fld']
        wa = cosmo.pars['wa_fld']
        # self.model must be in param file
        imodel = get_imodel(self.model)

        if self.model == 'axion':
            if wa != 0:
                raise ValueError('For axion, wa must be set to 0.')
            return np.log(self.Pw0(w0, imodel))
        else:
            return np.log(self.Pw0wa(w0, wa, imodel))
