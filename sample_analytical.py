#!/usr/bin/python
"""
This script is used to obtain the sampled w0-wa distributions,
following the anlytical expressions defined in the paper, with
the accuracy set there.
"""

from analyze_coeffs import Analyze
from scipy.integrate import trapz
import numpy as np
import os
import sys
import common as co


###########################
# Define things
###########################

def exp_fit(X, c0, c1, c2):
    return c0 * np.exp(-np.abs(X  / c2)**c1)

def exp_fit_sum(X, c0, c1, c2, c3, c4, c5):
    return exp_fit(X, c0, c1, c2) + exp_fit(X, c3, c4, c5)

def exp_gauss(X, c0, c2):
    return c0 * np.exp(-np.abs(X / c2)**2)

def gauss_dist(X, sigma):
    return np.exp(-0.5 * np.abs(X / sigma) ** 2) / (sigma * np.sqrt(2 * np.pi))

def poly(X, c):
    return np.sum([ci * X ** i for i, ci in enumerate(c)], axis=0)

output_folder = sys.argv[1]

models_dirs = [
###  'quintessence_axion-fit-w-1-Taylor_legacy-(1-a)-26331/',  # Axion fit no CLASS
 'quintessence_axion-fit-w-1-Taylor_legacy-(1-a)-41436/',  # Axion fit with CLASS

### 'quintessence_monomial-ints-fit-w-2-Taylor_legacy-(1-a)-36773/',  # Monomial fit no CLASS
# 'quintessence_monomial-ints-fit-w-2-Taylor_legacy-(1-a)-41160',  # Monomial fit with CLASS
 'quintessence_monomial-ints-fit-w-2-Taylor_legacy-(1-a)-41160-GAUSS-analytical',  # Monomial fit with CLASS

 ## 'quintessence_eft-logPhi-fit-w-2-Taylor_legacy-(1-a)-31906', # EFT fit no CLASS
# 'quintessence_eft-logPhi-fit-w-2-Taylor_legacy-(1-a)-41270/', # EFT fit with CLASS
 'quintessence_eft-logPhi-fit-w-2-Taylor_legacy-(1-a)-41270-GAUSS-analytical/', # EFT fit with CLASS

### 'quintessence_modulus-fit-w-2-Taylor_legacy-(1-a)-36771/' # Modulus fit no CLASS
# 'quintessence_modulus-fit-w-2-Taylor_legacy-(1-a)-41269/' # Modulus fit with CLASS
 'quintessence_modulus-fit-w-2-Taylor_legacy-(1-a)-41269-GAUSS-analytical/' # Modulus fit with CLASS
    ]

###########################
# Analytical values
###########################

Pw0_c = np.array([
    # A, exponent, sigma, A2, exponent, sigma2
    #### [332, 1.80, 3.77e-3, 0, 0, 1e-100], # Axion fit no CLASS
    [737, 0.775, -1.08e-3, 0, 0, 1e-100], # Axion fit with CLASS

    #### [8.30, 0.729, 0.101, 0, 0, 1e-100],  # Monomial fit no CLASS
    [8.21, 0.734, 0.102, 0, 0, 1e-100],  # Monomial fit with CLASS

    #### [1.06, 1.76, 0.387, 12.4, 1.03, 0.0518],  # EFT fit full w0 (no CLASS)
    [12.9, 1.01, 0.0499, 1.05, 1.76, 0.383],  # EFT fit full w0 (with CLASS)

    #### # [9.55, 0.626, 0.0711, 0, 0, 0]  # Modulus old (fit no CLASS)
    #### [8.18, 0.914, 0.0699, 0.579, 3.65, 0.776]  # Modulus fit no CLASS
    [8.13, 0.890, 0.0712, 0.535, 3.95, 0.803]  # Modulus fit with CLASS
])

mean_wa_c = np.array([
    [0, 0, 0],  # Axion fit no CLASS

    #### [-1.26, -1.13, 0.125],  # Monomial fit no CLASS
    [-1.28, -1.21, 0.0564],  # Monomial fit with CLASS

    #### [-1.42, -1.29, 0.133], # EFT no CLASS
    [-1.43, -1.35, 0.0882], # EFT fit with CLASS

    #### # [-0.926, -0.319, 0.613] # Modulus old fit no CLASS
    #### [-0.954, -0.457, 0.491] # Modulus fit no CLASS
    [-0.946, -0.460, 0.481] # Modulus fit with CLASS
])

# wa = 0 for Axion
# Pwaw0_c = np.array([
#     # A x sigma, sigma_0, sigma_1, sigma_2
#     [0, 0.1, 0.1, 0.1],  # Axion fit no CLASS
#
#     #### [0.572, 0.124, 0.192, 0.0737],  # Monomial fit no CLASS
#     [0.568, 0.107, 0.147, 0.0458],
#
#     #### # [0.577, 0.203, 0.372, 0.173],  # EFT fit w0 < -0.7 (fit no CLASS)
#     #### [0.585, 0.0818, 0.0846, 0.00385],  # EFT fit w0 < -0.4 (fit no CLASS)
#     [0.585, 0.0959, 0.124, 0.0304],  # EFT fit w0 < -0.4 (fit with CLASS)
#
#     #### # [0.573, 0.214, 0.425, 0.211] # Modulus old no CLASS
#     #### [0.567, 0.102, 0.166, 0.0878]  # Modulus no CLASS
#     [0.569, 0.104, 0.162, 0.0815]  # Modulus fit with CLASS
# ])

#### Fit with Gaussian ####
Pwaw0_c = np.array([
    #  sigma_0, sigma_1, sigma_2
    [0.1, 0.1, 0.1],  # Axion fit no CLASS

    [0.0769, 0.108, 0.0357],  # Monomial

    [0.0556, 0.0577, 0.00320],  # EFT

    [0.0755, 0.121, 0.0628]  # Modulus fit with CLASS
])


###########################
# Define functions
###########################

def get_imodel(model):
    d = {'axion': 0,
        'monomial': 1,
         'eft': 2,
         'modulus':3}
    return d[model]

def Pw0(w0, imodel):
    if w0 < -1:
        return 0

    return exp_fit_sum(w0 + 1, *Pw0_c[imodel])

# Old fit not exactly Gaussian
# def sigma_w0(w0, imodel):
#     scoeffs = Pwaw0_c[imodel, 1:]
#     return poly(w0, scoeffs)
#
# def Pwa_w0(wa, w0, imodel):
#     Asigma = Pwaw0_c[imodel, 0]
#     sigma = sigma_w0(w0, imodel)
#     A = Asigma / sigma
#     mean_wa = poly(w0, mean_wa_c[imodel])
#     if wa > 0:  # This might need to be changed
#         return 0
#     return exp_gauss(wa - mean_wa, A, sigma)

# New fit exactly Gaussian
def sigma_w0(w0, imodel):
    scoeffs = Pwaw0_c[imodel]
    return poly(w0, scoeffs)

def Pwa_w0(wa, w0, imodel):
    sigma = sigma_w0(w0, imodel)
    mean_wa = poly(w0, mean_wa_c[imodel])
    if wa > 0:  # This might need to be changed
        return 0
    return gauss_dist(wa - mean_wa, sigma)

def Pw0wa(w0, wa, imodel):
    return Pw0(w0, imodel) * Pwa_w0(wa, w0, imodel)

#############################
# Check normalization
#############################

from scipy.integrate import quad
def int_Pw0_dw0(imodel):
    return quad(Pw0, -1, np.inf, args=(imodel), limit=100, epsrel=1e-4, epsabs=1e-5)[0]

def int_Pwa_w0_dwa(w0, imodel):
    return quad(Pwa_w0, -np.inf, np.inf, args=(w0, imodel), limit=100, epsrel=1e-4, epsabs=1e-5)[0]

def int_Pw0wa_dw0(wa, imodel):
    return quad(Pw0wa, -1, np.inf, args=(wa, imodel), limit=100, epsrel=1e-4, epsabs=1e-5)[0]

def int_int_Pw0wa_dw0_dwa(imodel):
    return quad(int_Pw0wa_dw0, -np.inf, np.inf, args=(imodel), limit=100, epsrel=1e-4, epsabs=1e-5)[0]

print("##############################")
print("##############################")
print("Check normalization of P[w0, wa]")
print("Axion: ", int_Pw0_dw0(0))
print("Monomial: ", int_int_Pw0wa_dw0_dwa(1))
print("EFT: ", int_int_Pw0wa_dw0_dwa(2))
print("Modulus: ", int_int_Pw0wa_dw0_dwa(3))
print("")
print("##############################")
print("##############################")
print("Check normalization of P[w0]")
print("Axion: ", int_Pw0_dw0(0))
print("Monomial: ", int_Pw0_dw0(1))
print("EFT: ", int_Pw0_dw0(2))
print("Modulus: ", int_Pw0_dw0(3))
print("")
print("##############################")
print("##############################")

print("Check normalization of P[wa|w0 = -0.7]")
print("Monomial: ", int_Pwa_w0_dwa(-0.7, 1))
print("EFT: ", int_Pwa_w0_dwa(-0.7, 2))
print("Modulus: ", int_Pwa_w0_dwa(-0.7, 3))
print("")
print("##############################")
print("##############################")


def getGelmanRubin(chains):
    """
    Return an array with the R-1 (Gelman-Rubin criterion with the
    t-Student dimensional estimator correction) for each parameter.
    (Based on: https://blog.stata.com/2016/05/26/gelman-rubin-convergence-diagnostic-using-multiple-chains/,
    and http://www.stat.columbia.edu/~gelman/research/published/brooksgelman2.pdf)
    MontePython = Bool. If true, return R - 1 computed (almost) as in
    MontePython, i.e. R = Between / Within variances; except for the
    weighting for the chain length.

    """

    varOfWalker = np.var(chains, axis=2, ddof=1)
    meanOfWalker = np.mean(chains, axis=2)

    W = np.mean(varOfWalker, axis=0)
    b = np.var(meanOfWalker, axis=0, ddof=1)  # b = B/n

    M, _, N = [float(i) for i in chains.shape] # M chains, 2 dof, N elemnts in chain

    V = (N - 1) / N * W + (M + 1) / M * b

    # return np.sqrt((d + 3) / (d + 1) * V / W) - 1  # small correction neglected
    return V / W - 1  # MontePython style

def sample_grid(size, imodel):
    stored = 0
    sample = np.empty((2, size))

    w0_list = np.linspace(-1, 0, 10000)
    wa_list = np.linspace(-1, 0, 10000)
    N = np.max([trapz(Pw0wa(w0, wa_list, imodel)) for w0 in w0_list])

    w0_0 = -0.99
    wa_0 = 0
    while stored < size:
        w0 = np.random.normal(w0_0, 0.1)
        wa = np.random.normal(wa_0, 0.1)

        if np.random.uniform(0, 1) < Pw0wa(w0, wa, imodel) / N:
            sample[:, stored] = (w0, wa)
            stored += 1
            w0_0 = w0
            wa_0 = wa
            print('w0, wa = {}, {}'.format(w0, wa))
            print('P(w0, wa) = {}'.format(Pw0wa(w0, wa, imodel) / N))
            print('{} of {}'.format(stored, size))

    return sample

def sample_MH(size, imodel, maxsteps=1e7, par=10, ftemp='', burnin=None):
    size /= par
    stored = 0
    stored_old = size
    count = 0
    sample = np.empty((par, 2, int(maxsteps)))

    if burnin is None:
        burnin = size

    if imodel == 1: # monomial
        step_w0 = 2 * Pw0_c[imodel, 2] # 0.01
        step_wa = Pwaw0_c[imodel, 1]  # 0.01
    elif imodel == 2: # eft
        step_w0 = 2 * Pw0_c[imodel, -1] # 0.01
        step_wa = Pwaw0_c[imodel, 1]  # 0.01
    elif imodel == 3: # modulus
        step_w0 = 3 * Pw0_c[imodel, -1] # 0.01
        step_wa = 2 * Pwaw0_c[imodel, 1]  # 0.01

    w0_0 = np.random.uniform(-1, Pw0_c[imodel, -1] * 2, size=par)
    wa_0 = poly(w0_0, mean_wa_c[imodel])
    # w0_0 = w0_0 + np.random.uniform(-step_w0, step_w0, size=par)
    # wa_0 = wa_0 + np.random.uniform(-step_wa, step_wa, size=par)
    # w0_0 = w0_0 + np.random.normal(w0_0, step_w0, size=par)
    # wa_0 = wa_0 + np.random.normal(wa_0, step_wa, size=par)
    P0 = np.array([Pw0wa(w0_0[i], wa_0[i], imodel) for i in range(par)])

    converge = False
    while (((stored < size) or (not converge))) and (count < maxsteps):
        count += 1
        w0 = w0_0 + np.random.uniform(-step_w0, step_w0, size=par)
        wa = poly(w0, mean_wa_c[imodel]) + np.random.uniform(-step_wa, step_wa, size=par)
        # w0 = np.random.normal(w0_0, step_w0, size=par)
        # wa = np.random.normal(wa_0, step_wa, size=par)

        P = np.array([Pw0wa(w0[i], wa[i], imodel) for i in range(par)])
        # P[(w0 > 0) + (wa < -1)] = 0

        accepted = np.random.uniform(0, 1) < (P / P0)

        P0[accepted] = P[accepted]
        w0_0[accepted] = w0[accepted]
        wa_0[accepted] = wa[accepted]

        if count < burnin:  # Remove burn-in
            print('Burn-in: {} of {}'.format(count, burnin))
            continue

        sample[accepted, :, stored] = np.array([w0[accepted], wa[accepted]]).T
        sample[~accepted, :, stored] = np.array([w0_0[~accepted], wa_0[~accepted]]).T
        stored += 1

        if converge:
            print('Sampling: {} of {} per chain'.format(stored, size))

        if (stored >= 2 * stored_old) and (not converge):
            Rm1 = getGelmanRubin(sample[:, :, :stored])
            converge = (np.max(Rm1) < 0.01)
            stored_old = stored
            print('R - 1 = {}'.format(Rm1))
            if ftemp != '':
                np.savez_compressed(ftemp, sample[:, :, :stored])
            # if converge:
            #     sample = np.empty((par, 2, size))
            #     stored = 0



    print('R - 1 = {}'.format(getGelmanRubin(sample[:, :, :stored])))
    print('step_w0 = {}'.format(step_w0))
    print('step_wa = {}'.format(step_wa))
    print('Acceptance ratio = {}'.format(np.max(stored) / float(count)))
    if count >= maxsteps:
        print('Max steps reached')

    if (stored > stored_old) and (ftemp != ''):
        np.savez_compressed(ftemp, sample[:, :, :stored])

    result = np.concatenate(sample[:, :, :stored], axis=1)

    # result = np.concatenate(sample[:, :, -int(size/par):], axis=1)[:, :size]

    # result = np.empty((2, size))
    # ini = 0
    # for i, chain in enumerate(sample):
    #     end = stored[i]
    #     if end >= size:
    #         end = size
    #     result[:, ini : end + ini] = chain[:, :end]
    #     ini = end

    return result

def sample_w0_MH(size, imodel, maxsteps=1e6, par=10, ftemp='', burnin=None):
    stored = 0
    stored_old = 10000
    count = 0
    sample = np.empty((par, 1, int(maxsteps)))

    if burnin is None:
        burnin = size

    step_w0 = Pw0_c[imodel, 2] / 6. # 0.01

    w0_0 = -0.99
    w0_0 = w0_0 + np.random.uniform(-step_w0, step_w0, size=par) # np.random.normal(w0_0, 0.01)
    P0 = np.array([Pw0(w0_0[i], imodel) for i in range(par)])

    converge = False
    while (((stored < size) or (not converge))) and (count < maxsteps):
        count += 1
        w0 = w0_0 + np.random.uniform(-step_w0, step_w0, size=par) # np.random.normal(w0_0, 0.01)

        P = np.array([Pw0(w0[i], imodel) for i in range(par)])
        # P[(w0 > 0)] = 0

        accepted = np.random.uniform(0, 1) < (P / P0)

        P0[accepted] = P[accepted]
        w0_0[accepted] = w0[accepted]

        if count < burnin:  # Remove burn-in
            print('Burn-in: {} of {}'.format(count, burnin))
            continue

        sample[accepted, :, stored] = np.array([w0[accepted]]).T
        sample[~accepted, :, stored] = np.array([w0_0[~accepted]]).T
        stored += 1

        if converge:
            print('Sampling: {} of {} per chain'.format(stored, size))

        if (stored > 2 * stored_old) and (not converge):
            Rm1 = getGelmanRubin(sample[:, :, :stored])
            converge = (np.max(Rm1) < 0.01)
            stored_old = stored
            print('R - 1 = {}'.format(Rm1))
            if ftemp != '':
                np.savez_compressed(ftemp, sample[:, :, :stored])
            # if converge:
            #     sample = np.empty((par, 1, size))
            #     stored = 0



    print('R - 1 = {}'.format(getGelmanRubin(sample[:, :, :stored])))
    print('step_w0 = {}'.format(step_w0))
    print('Acceptance ratio = {}'.format(np.max(stored) / float(count)))
    # result = np.concatenate(sample[:, :, -int(size/par):], axis=1)[:, :size]

    if (stored > stored_old) and (ftemp != ''):
        np.savez_compressed(ftemp, sample[:, :, :stored])

    result = np.concatenate(sample[:, :, :stored], axis=1)

    return result

def get_subsample(sample, size):
    sel = np.random.choice(sample.shape[1], size, replace=False)
    return sample[:, sel]


##############################################
# Function to create new params
##############################################
def compute_class_params_sampled(data, class_params):
    """
    Create a list of params dictionaries for CLASS input with h, and Omega_cdm varied.
    data.shape = nmodels x ncoeffs
    """
    #### We would have to change these if they are changed in computation.py ####

    params = \
        {"Omega_Lambda": 0,
        "Omega_scf": 0,
        # Omega_fld unespecified

        "use_ppf": "yes",

        # "fluid_equation_of_state": "CLP",  # For a newer version of CLASS
        "cs2_fld": 1,

        "output": class_params['output'],
        "z_max_pk": class_params['z_max_pk']
        }

    ldist = data.shape[0]
    h = np.random.uniform(0.6, 0.8, ldist)  # self.points_in_H_DA_f_plot)
    Omega0_cdm = np.random.uniform(0.15, 0.35, ldist)  #, self.points_in_H_DA_f_plot)


    params_ar = []
    for i, coeffs in enumerate(data):
        pdic = params.copy()
        pdic.update({'w0_fld': coeffs[0],
                     'wa_fld': coeffs[1],
                     'h': h[i],
                     'Omega_cdm': Omega0_cdm[i]
        })
        params_ar.append(pdic)

    return params_ar

###########################
# Debug
###########################
# from matplotlib import pyplot as plt
# w0 = np.linspace(-1, 0, 1000)
# wa = np.linspace(-1, 0, 1000)
# plt.plot(wa, Pw0(-0.9, 1) * Pwa_w0(wa, -0.9, 1))
# plt.plot(wa, Pw0wa(-0.9, wa, 1), '--')
# plt.plot(wa, Pw0(-0.85, 1) * Pwa_w0(wa, -0.85, 1))
# plt.plot(wa, Pw0wa(-0.85, wa, 1), '--')
# plt.plot(wa, Pw0(-0.8, 1) * Pwa_w0(wa, -0.8, 1))
# plt.plot(wa, Pw0wa(-0.8, wa, 1), '--')
# plt.plot(wa, Pw0(-0.75, 1) * Pwa_w0(wa, -0.75, 1))
# plt.plot(wa, Pw0wa(-0.75, wa, 1), '--')
# # plt.plot(wa, Pw0(-0.7, 1) * Pwa_w0(wa, -0.7, 1))
# # plt.plot(wa, Pw0(-0.65, 1) * Pwa_w0(wa, -0.65, 1))
# # plt.plot(wa, Pw0(-0.6, 1) * Pwa_w0(wa, -0.6, 1))
# # plt.plot(wa, Pw0(-0.55, 1) * Pwa_w0(wa, -0.55, 1))
# # plt.plot(wa, Pw0(-0.5, 1) * Pwa_w0(wa, -0.5, 1))
# plt.show()
# plt.close()
# print(Pw0(-0.6, 1) / Pw0(-0.9, 1))
# print(Pw0(-0.8, 1) / Pw0(-0.9, 1))
# sys.exit()


###########################
# Sample
###########################

# header = 'Sampled with analytical prior distributions.\n'
# for imodel, model_dir in enumerate(models_dirs):
#     analyze = Analyze(os.path.join(output_folder, model_dir),
#                          basis_cut=0)
#     ftemp = analyze.filepaths['analytical'][:-3] + 'npz'
#     size = 20000  # analyze.points_in_H_DA_f_plot
#     if 'axion' in model_dir:
#         result = sample_w0_MH(size, imodel, ftemp=ftemp)
#         header2 = 'c_0'
#     else:
#         result = sample_MH(size, imodel, ftemp=ftemp)
#         header2 = 'c_0 c_1'
#
#     np.savetxt(analyze.filepaths['analytical'], result.T, header=header + header2)


header = 'Sampled with analytical prior distributions.\n'
model = sys.argv[2]
imodel = get_imodel(model)
model_dir = models_dirs[imodel]

analyze = Analyze(os.path.join(output_folder, model_dir),
                     basis_cut=0, class_params_parametrization=co.class_params_CPL_fld)

if not os.path.isfile(analyze.filepaths['analytical']):
    ftemp = analyze.filepaths['analytical'][:-4] + '-chains.npz'
    size = analyze.points_in_H_DA_f_plot
    if 'axion' in model_dir:
        result = sample_w0_MH(size, imodel, ftemp=ftemp, burnin=10 * size)
        header2 = 'c_0'
    else:
        result = sample_MH(size, imodel, ftemp=ftemp, burnin=size)
        header2 = 'c_0 c_1'

    result = get_subsample(result, size)
    np.savetxt(analyze.filepaths['analytical'], result.T, header=header + header2)
    # np.savez_compressed(analyze.filepaths['analytical'], result)

if not os.path.isfile(analyze.filepaths['params_analytical']):
    header = '# Dictionary of params to use with cosmo.set() and sampled analytical coefficients'
    data = np.loadtxt(analyze.filepaths['analytical'])  # (nmodels, ncoeffs)
    if len(data.shape) == 1:
        data = np.array([data, np.zeros(data.size)]).T
    class_params = analyze.class_params[0]
    params = compute_class_params_sampled(data, class_params)
    with open(analyze.filepaths['params_analytical'], 'w') as f:
        f.write(header + '\n')
        for p in params:
            f.write(str(p) + '\n')
    #######
    # Debug
    #######
    # from classy import Class
    # from common import compute_observables_th
    # cosmo = Class()
    # cosmo.set(params[0])
    # cosmo.compute()
    # # print(np.any(cosmo.get_background()['w_smg'] < -1))
    # print(compute_observables_th(cosmo, 10))
    # cosmo.struct_cleanup()



