RUFIAN is a code that RUn simulations, FIt parametrizations and ANalyze them.

Super-preliminary, undocumented

Dependencies:
  - work-in-class
  - numpy
  - scipy
  - matplotlib
  - mpi4py


Quick note about files:
  - `computation.py`: Define here your model and parameter priors
  - `compute_analysis_results.py`: Analyze the results (get observables, etc.)
  - `compute_analysis_analytical.py`: Analyze the sample from analytical priors (get observables, etc.)
  - `sample_analytical.py`: Define analytical distributions and sample in them
  - `binning.py`: Here the fitting is implemented
  - `analyze_coeffs.py`: Here the methods to analyze fit are implemented
  - `common.py`: Here there are some functions common among files.

The plotting utilities might not work as expected.

This code has been used to produce <em>Theoretical priors in scalar-tensor cosmologies: Thawing quintessence</em> (Phys.Rev.D 101 (2020) 6, 063508)
You can cite this work copying the following bibentry
```
    @article{Garcia-Garcia:2019cvr,
        author = "García-García, Carlos and Bellini, Emilio and Ferreira, Pedro G. and Traykova, Dina and Zumalacárregui, Miguel",
        title = "{Theoretical priors in scalar-tensor cosmologies: Thawing quintessence}",
        eprint = "1911.02868",
        archivePrefix = "arXiv",
        primaryClass = "astro-ph.CO",
        doi = "10.1103/PhysRevD.101.063508",
        journal = "Phys. Rev. D",
        volume = "101",
        number = "6",
        pages = "063508",
        year = "2020"
    }
```

Code name by Raúl Rodríguez Segundo (IFF - CSIC)
