#!/usr/bin/python2.7
from analyze_coeffs import Analyze
from getdist import plots
from matplotlib import pyplot as plt
from work_in_class import wicmath
import common as co
import getdist as GD
import numpy as np
import os
import sys


class Plot_figures(Analyze):
    def __init__(self, preroot, fit_function=None, time_variable=None, no_obs=False, show=False,
                 fig_output_format='pdf', densitylog=False,
                 remove_crazy_functions=None,
                 transform=[None, None, None],
                 run_residuals=True,
                 only_residuals=False,
                 run_check_dim_reduction_sampling=True,
                 run_rebinned_histograms=True,
                 **kwargs):

        Analyze.__init__(self, preroot, fit_function, time_variable, no_obs, **kwargs)

        if no_obs:
            self.outdir_figs = os.path.join(self.root, 'figures-no_obs')
        else:
            self.outdir_figs = os.path.join(self.root, 'figures')
        if not os.path.isdir(self.outdir_figs):
            os.makedirs(self.outdir_figs)

        self.fig_output_format = fig_output_format
        self.densitylog = densitylog
        self.show = show

        params = kwargs.keys()

        if remove_crazy_functions == None:
            pass
        elif callable(remove_crazy_functions):
            self.set_select_crazy_functions(remove_crazy_functions)
        else:
            raise TypeError('remove_crazy_functions must be a function that selects crazy functions!')

        if transform == [None, None, None]:
            pass
        elif callable(transform[0]) and callable(transform[1]):
            self.set_transform_function(transform[0], transform[1], transform[2])
        else:
            raise TypeError('transform must be an array of [transform_function, transform_function_inverse, transform_function_label]')

        self.prepare_histograms(run_residuals, only_residuals,
                                run_check_dim_reduction_sampling,
                                run_rebinned_histograms)

    def _close_fig(self, plt, f, suffix):
        if 'sampling' in suffix:
            if self.remove_out_of_bounds:
                suffix += '-removed-out-of-bounds'
                removed_part = r'. Removed out-of-bounds sampled models in orig scale ({:.2f}% data)'.format(self.percentage_ofb_removed)

            # elif self.remove_outliers_sigma:
            #    suffix += '-removed-{}-sigma-outliers'.format(self.remove_outliers_sigma)

            #    removed_part = r'. Removed outliers from orig data at > ${} \sigma$ ({:.2f}% data)'.format(self.remove_outliers_sigma, self.percentage_outliers_removed)
            elif self.remove_crazy_functions:
                suffix += '-removed-crazy-functions'
                removed_part = r' Removed {:.2f}% of the sampled models'.format(self.percentage_remove_crazy_functions)

            else:
                removed_part = ''

            suffix += self.suffix_sampled

            try:
                f.suptitle(f._suptitle.get_text() + removed_part)
            except AttributeError:
                f.suptitle(removed_part)

        if f._suptitle:
            plt.tight_layout(rect=[0, 0.03, 1, 0.93])
        else:
            plt.tight_layout()

        suffix += '.{}'.format(self.fig_output_format)

        outpath = os.path.join(self.outdir_figs, self.modelname + suffix)
        sys.stdout.write("Saving figure: {}\n".format(outpath))
        plt.savefig(outpath)
        if self.show:
            plt.show()
        plt.close()

    def _plot_log_hists(self, dat, name, ax, hist_num):
        colors = ['darkblue', 'red', 'darkgreen']
        c = 0
        # bins = np.linspace(np.log10(np.min(np.abs(dat[dat != 0]))), np.log10(np.max(np.abs(dat))), self.nbins)
        for i in [np.log10(dat[dat > 0]), np.log10(-dat[dat < 0]), dat[dat == 0]]:
            labels = [name + '> 0', name + '< 0', name + '= 0']
            labels = [r'${}$'.format(lbl) for lbl in labels]
            h, x = np.histogram(i, bins=self.nbins, density=True)
            if hist_num == 0:
                ax.bar(x[:-1], h, width=np.diff(x), align='edge', alpha=1-0.25*c, label=labels[c])
            else:
                ax.plot(x[:-1] + 0.5 * np.diff(x), h, c=colors[c])
            c += 1

    def _plot_lin_hists(self, dat, name, ax, hist_num):
        # bins = np.linspace(np.min(dat), np.max(dat), self.nbins)
        H, x = np.histogram(dat[~np.isnan(dat)], bins=self.nbins) #, density=True)
        bin_width = np.diff(x[:2])
        h = H/float(len(dat) * bin_width)
        h_nan = np.isnan(dat).sum()/float(len(dat) * bin_width)

        if hist_num == 0:
            ax.bar(x[:-1], h, width=np.diff(x), align='edge')
            ax.bar(min(x) - 6*bin_width, h_nan, width=bin_width, align='edge', label='errors', color='r')
        else:
            ax.plot(x[:-1] + 0.5 * np.diff(x), h, c='darkblue')
            ax.bar(min(x) - 6*bin_width, h_nan, width=bin_width, align='edge', label='errors', color='darkred', alpha=0.3)

    def _plot_histograms_comparison(self, axs, hists_to_plot):
        """
        Plot up_to_two histograms.
        """
        #TODO: Bins should be shared by two histograms when being compared!!
        for i, histogram in enumerate(hists_to_plot):
            c = 0
            for dat, name, ax in zip(histogram.data, histogram.bins, axs.reshape(-1)):
                if self.histlogauto and (self.hist_diagonal_sampled in hists_to_plot):
                    if self._hist_diagonal_sampled_scale[c] == 'log':
                        self._plot_log_hists(dat, name, ax, i)
                    else:
                        self._plot_lin_hists(dat, name, ax, i)
                elif self.histlog:
                    self._plot_log_hists(dat, name, ax, i)
                else:
                    self._plot_lin_hists(dat, name, ax, i)

                if histogram is hists_to_plot[-1]:
                    if self.histlog or ((self.hist_diagonal_sampled in
                                         hists_to_plot) and
                                        (self._hist_diagonal_sampled_scale[c] ==
                                         'log')):
                        ax.set_xlabel('$\log_{{10}}({})$'.format(name))
                    else:
                        ax.set_xlabel(r'${}$'.format(name))
                        ax.set_yscale(self.densitylog)
                    ax.set_ylabel('Density')
                    ax.legend(loc=0)
                c += 1

    def plot_residuals(self):
        self.residualsHist.compute(self.nbins)

        n_hists = len(self.residualsHist.data[:-1])

        f, axs = plt.subplots(n_hists/2, 2, figsize=(10, 1.5 * n_hists))
        ax = axs.reshape(-1)

        i = 0
        for Y, x in self.residualsHist.histograms[:-1]:
            ax[i].bar(x[:-1], Y/float(sum(Y)), width=np.diff(x), align='edge')
            ax[i].set_ylabel('Density')

            if i%2 == 0:
                ax[i].set_xlabel(r'$z$')

            i += 1

        for i, dat in enumerate(self.residualsHist.data[:-1]):
            if i%2 == 0:
                continue
            if i != n_hists - 1:
                p1 = sum(dat > -1) / float(len(dat)) * 100
                p2 = sum(dat > -2) / float(len(dat)) * 100
                text1 = '{:.0e} % > -1'.format(p1)
                text2 = '{:.0e} % > -2'.format(p2)
            else:
                p1 = sum(dat > np.log10(0.3/100.)) / float(len(dat)) * 100
                p2 = sum(dat > -3) / float(len(dat)) * 100
                text1 = '{:.0e} % > 0.3%'.format(p1)
                text2 = '{:.0e} % > -3'.format(p2)
            textstr = '\n'.join((text2, text1))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax[i].text(0.05, 0.95, textstr, transform=ax[i].transAxes, fontsize=12,
                    verticalalignment='top', bbox=props)

        axs[0,1].set_xlabel(r'$\log_{10}(max(rel. dev. H))$')
        axs[1,1].set_xlabel(r'$\log_{10}(max(rel. dev. D_A))$')
        axs[2,1].set_xlabel(r'$\log_{10}(max(rel. dev. f))$')
        axs[3,1].set_xlabel(r'$\log_{10}(rel. dev. D_A(rec))$')

        f.suptitle(self.model_title)
        self._close_fig(plt, f, '-residuals')

    def plot_histograms(self, run_raw_histograms=True,
                        run_rebinned_histograms=True,
                        run_diagonalized_histograms=True):

        histogram_array = []

        if run_raw_histograms:
            histogram_array.append((self.hist_orig_scale_orig, '', '-histogram_orig-scale'))
            if self.hist_corrected_scale_orig is not self.hist_orig_scale_orig:
                histogram_array.append((self.hist_corrected_scale_orig, '', '-histogram_corrected-scale'))
        if run_rebinned_histograms:
            histogram_array.append((self.hist_corrected_scale_rb_orig, ' Hist.rebin. s.t. var = 1',
                                    '-histogram_corrected-scale_rebbined'))
        if run_diagonalized_histograms:
            histogram_array.append((self.hist_diagonal_orig, 'Diag. basis', '-histogram-diagonalized'))

        for histogram, title, suffix in histogram_array:
            c = 0
            f, axs = plt.subplots(2, 6, figsize=(25, 10))
            for dat, name, ax in zip(histogram.data, histogram.bins, axs.reshape(-1)[:-1]):
                if self.histlogauto and (histogram is self.hist_diagonal_orig):
                    if self._hist_diagonal_sampled_scale[c] == 'log':
                        self._plot_log_hists(dat, name, ax, 0)
                    else:
                        self._plot_lin_hists(dat, name, ax, 0)
                elif self.histlog:
                    self._plot_log_hists(dat, name, ax, 0)
                else:
                    self._plot_lin_hists(dat, name, ax, 0)

                if self.histlog or ((histogram is self.hist_diagonal_orig) and
                                    (self._hist_diagonal_sampled_scale[c] ==
                                    'log')):

                    ax.set_xlabel('$\log_{{10}}({})$'.format(name))
                else:
                    ax.set_xlabel(r'${}$'.format(name))
                    ax.set_yscale(self.densitylog)
                ax.set_ylabel('Density')
                ax.legend(loc=0)
                c += 1

            f.suptitle(''.join([self.model_title, title]))
            f.delaxes(axs.reshape(-1)[-1])

            self._close_fig(plt, f, suffix)

    def plot_diagonal_basis_time_evolution(self):
        diagonal_basis = self.diagonal_basis

        jet = plt.get_cmap('tab10')
        colors = iter(jet(np.linspace(0, 1, 10)))

        f, ax = plt.subplots(1, 1)

        for i, e in enumerate(diagonal_basis):
            c = next(colors)
            plt.semilogy(self.lna, e, label='$e_{}$'.format(i), c=c)
            plt.semilogy(self.lna, -e, '-.', c=c)
        plt.ylabel('$e_i$')
        plt.xlabel(self.time_label)
        plt.legend(loc=0)
        self._close_fig(plt, f, '-diagonal-basis-time-evolution')

    def plot_relative_error_vs_dimensions_fitted_function(self, log_hist_threshold=0.5):
        if not len(self.relative_errors_vs_dimmensions_fitted_function):
            self.compute_relative_error_vs_dimensions_fitted_function()

        self._plot_relative_error_vs_dimensions(self.relative_errors_vs_dimmensions_fitted_function, 'fitted_function', self.fitted_function_label.replace('$', ''), log_hist_threshold)

    def plot_relative_error_vs_dimensions_observables(self, log_hist_threshold=0.5):
        if not len(self.obs_reldev['H']):
            self.compute_relative_error_vs_dimensions_observables()

        for reldev, suffix, label in ((self.obs_reldev['H'], 'H', 'H'),
                               (self.obs_reldev['DA'], 'DA', 'D_A'),
                               (self.obs_reldev['f'], 'f', 'f')):

            self._plot_relative_error_vs_dimensions(reldev, suffix, label, self.basis_cut, log_hist_threshold)

        reldev = self.obs_reldev['max']

        self._plot_relative_error_vs_dimensions(reldev, 'observables', r'\mathcal{O}', self.basis_cut, log_hist_threshold)

        self._plot_relative_error_vs_dimensions(self.obs_reldev['DA_rec'],
                                                'DA_rec',
                                                r'D_A(z_{rec})',
                                                self.basis_cut_DArec,
                                                log_hist_threshold)

    def _plot_relative_error_vs_dimensions(self, reldev, suffix, label, basis_cut, log_hist_threshold=0.5):
        hist_orig_scale_orig = self.hist_orig_scale_orig

        formula = r"max\left(\frac{{|{0}^{{exact}} - {0}^{{eigv}}|}}{{max(|{0}^{{exact}}|, |{0}^{{eigv}}|)}}\right)".format(label)
        xlabel = 'Number of diagonal coefficients used'

        f, axs = plt.subplots(2, 6, figsize=(25, 10)) # (len(reldev) + 1)/2)
        ax = axs.reshape(-1)[:-1]

        for i, maxreldevs in enumerate(reldev):
            # Note: When using all eigv, some reldevs will be exactly 0,
            # breaking np.log10. We decided to plot those at min(reldev)/10.
            # with different color & legend.
            selection = (maxreldevs != 0) & ~np.isnan(maxreldevs)
            h, x = np.histogram(np.log10(maxreldevs[selection]), bins=self.nbins, density=True)

            if np.any(h > log_hist_threshold):
                h, x = np.histogram(maxreldevs[~np.isnan(maxreldevs)], bins=self.nbins, density=True)
                ax[i].set_xlabel(r'${}$'.format(formula))
            else:
                ax[i].set_xlabel(r'$log_{{10}}\left( {} \right)$'.format(formula))

            ax[i].bar(x[:-1], h, width=np.diff(x), align='edge')

            bin_width = np.diff(x[:2])

            if 0 in maxreldevs:
                Y0 = sum(maxreldevs == 0) / (float(len(maxreldevs)) * bin_width)
                ax[i].bar(min(x) - 6*bin_width, Y0, width=bin_width, align='edge', label='exactly 0', color='r')
                ax[i].legend(loc=0)

            if np.any(np.isnan(maxreldevs)):
                Y0 = sum(np.isnan(maxreldevs)) / (float(len(maxreldevs)) * bin_width)
                ax[i].bar(min(x) - 3*bin_width, Y0, width=bin_width, align='edge', label='error ({:.0e}%)'.format(Y0[0]*100), color='green')
                ax[i].legend(loc=0)

            ax[i].set_ylabel('Density')
            ax[i].set_title('Dimensions used: {}'.format(i + 1))

            # Set 0's to 20 orders of magnitude lower than minimum value in
            # hist_orig_scale_orig so that we can use np.log10 afterwards.
            if np.any(maxreldevs != 0):
                maxreldevs[maxreldevs == 0] = min(maxreldevs[maxreldevs != 0]) * 1e-20

        self._close_fig(plt, f, '-relative-difference-number-dimensions-histograms-' + suffix)

        medians = np.nanmedian(reldev, axis=1)
        lower = np.nanmin(reldev,axis=1)
        upper = []
        for i in reldev:
            index_cut = int(self.acceptance * len(i) + 0.5)
            i = i[~np.isnan(i)]
            if index_cut == 0:
                upper.append(np.max(np.sort(i)))
            else:
                upper.append(np.max(np.sort(i)[:-index_cut]))

        upper = np.array(upper)
        # upper = np.max(upper, axis=1)

        f, ax = plt.subplots(1, 1)
        plt.errorbar(np.arange(len(hist_orig_scale_orig.data)) + 1 , medians,
                     np.abs([medians-lower, medians-upper]), fmt='o')
        plt.xlabel(xlabel)
        ylabel = r'median$\left[{}\right] \pm$ accepted range'.format(formula)
        plt.ylabel(ylabel)
        self._close_fig(plt, f, '-relative-difference-number-dimensions-linear-' + suffix)

        # Note that (0 + 0 + 0 + ... + 0 + 1) / 100 = 1e-2, not a good variable.
        # Use np.log10(reldev) for a more informative average.
        medians = np.nanmedian(np.log10(reldev), axis=1)
        f, ax = plt.subplots(1, 1)
        plt.errorbar(np.arange(len(hist_orig_scale_orig.data)) + 1, medians,
                     np.abs([np.log10(lower)-medians, medians-np.log10(upper)]), fmt='o')  # ([0]*len(hist_orig_scale_orig.data), sigmas)
        ax.axhline(np.log10(basis_cut), ls=':',label='Max. err. = {:.0e}'.format(basis_cut))
        #plt.yscale('log')
        plt.grid(True)
        plt.xlabel(xlabel)
        plt.ylabel(r'median$\left[log_{{10}}\left( {} \right)\right] \pm$ accepted range'.format(formula))
        plt.title(r"0's changed to min(residual)$10^{-20}$ to avoid $\infty$'s")
        plt.legend(loc=0)
        sys.stdout.write("0's changed to min(rel.dev)*1e-20 to be able to use np.log10\n")
        self._close_fig(plt, f, '-relative-difference-number-dimensions-log-' + suffix)

    def plot_fitted_function_from_reduced_space(self, index):
        orig_coeffs = self.hist_orig_scale_orig.data[:,index]
        diagonal_orig_coeffs = self.hist_diagonal_orig.data[:, index]
        _fit_function = self._fit_function

        selection = [False]*len(orig_coeffs)
        reldev = []

        f, ax = plt.subplots(2, 1, figsize=(4, 7))

        Fint_tmp = _fit_function(self.time_array, orig_coeffs)
        ax[0].plot(self.lna, Fint_tmp, c='black', label='orig.')
        ax[1].plot(self.lna, np.zeros(len(self.lna)), c='black', label='orig')

        for i in range(len(orig_coeffs)):
            selection[i] = True

            corrected_scale_orig_reduced_coeffs = \
                    self.hist_corrected_scale_means + \
                    self.transfMatrix[selection].T.dot(diagonal_orig_coeffs[selection][:, None])

            data = corrected_scale_orig_reduced_coeffs

            if self.histlogcomplex:
                data_reals = data[:len(data)/2]
                data_imag = data[len(data)/2:]
                orig_scale_reduced_data = wicmath.log_complex_inverse(data_reals + data_imag * 1j)
            elif self.histtransform:
                orig_scale_reduced_data = self._transform_function_inverse(data)
            else:
                orig_scale_reduced_data = data

            Fint_red_tmp = _fit_function(self.time_array, data.flatten())

            ax[0].plot(self.lna, Fint_red_tmp, '--', label='{} eigv.'.format(i + 1), alpha=0.8)

            reldev = np.abs(Fint_red_tmp - Fint_tmp) / \
                np.max([np.abs(Fint_tmp), np.abs(Fint_red_tmp)], axis=0)
            ax[1].plot(self.lna, reldev, '--', label='{} eigv.'.format(i + 1), alpha=0.8)


        ax[0].set_xlabel(self.time_label)
        ax[1].set_xlabel(self.time_label)

        ax[0].set_ylabel(self.fitted_function_label)
        formula = r"$\frac{f_{exact} - f_{eigv}|}{max(|f_{exact}|, |f_{eigv}|))}$"
        ax[1].set_ylabel(formula)

        ax[1].set_yscale('log')

        ax[0].legend(loc=0)
        f.suptitle("Function's index: {}".format(index))
        self._close_fig(plt, f, '-function-from-reduced-space-index-{}'.format(index))

    def plot_check_dimensional_reduction(self):
        f, axs = plt.subplots(2, 6, figsize=(25, 10))

        self._plot_histograms_comparison(axs, [self.hist_orig_scale_orig, self.hist_orig_scale_orig_reduced])

        f.suptitle(self.model_title + r' Dim. red. ({}D, Error in observables $< {:.1E}$)'.format(self.dim_red, self.basis_cut))
        f.delaxes(axs.reshape(-1)[-1])
        self._close_fig(plt, f, '-check-dimensional-reduction_orig-scale')

        if self.hist_orig_scale_orig is self.hist_corrected_scale_orig:
            return

        f, axs = plt.subplots(2, 6, figsize=(25, 10))

        self._plot_histograms_comparison(axs, [self.hist_corrected_scale_orig, self.hist_corrected_scale_orig_reduced])

        f.suptitle(self.model_title + r' Dim. red. ({}D, Error in observables $< {:.1E}$)'.format(self.dim_red, self.basis_cut))
        f.delaxes(axs.reshape(-1)[-1])
        self._close_fig(plt, f, '-check-dimensional-reduction_corrected-scale')

    def plot_check_dimensional_reduction_sampling(self):
        f, axs = plt.subplots(2, 6, figsize=(25, 10))

        self._plot_histograms_comparison(axs, [self.hist_orig_scale_orig,
                                               self.hist_orig_scale_sampled])


        f.delaxes(axs.reshape(-1)[-1])
        f.suptitle('Comparison original hists. and sampled (in diag. basis) ones in original basis', size=20)
        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling_orig-scale')

        if self.hist_orig_scale_sampled is self.hist_corrected_scale_sampled:
            return

        f, axs = plt.subplots(2, 6, figsize=(25, 10))

        self._plot_histograms_comparison(axs,
                                         [self.hist_corrected_scale_orig_reduced,
                                          self.hist_corrected_scale_sampled])


        f.delaxes(axs.reshape(-1)[-1])
        f.suptitle('Comparison original hists. and sampled (in diag. basis) ones in original basis', size=20)
        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling_corrected-scale')

    def plot_check_sampling_histograms(self):
        f, axs = plt.subplots(2, 6, figsize=(25, 10))

        self._plot_histograms_comparison(axs, [self.hist_diagonal_orig, self.hist_diagonal_sampled])

        f.delaxes(axs.reshape(-1)[-1])
        f.suptitle('Comparison original hists. and sampled ones in diagonal basis', size=20)
        self._close_fig(plt, f, '-check-sampling-histogram_diagonal')

    def plot_fitted_functions_original_and_sampled(self):
        if not len(self.fitted_functions_sampled):
            self.compute_fitted_functions_original_and_sampled()

        Fint = self.fitted_functions
        Fint_red = self.fitted_functions_sampled

        f, ax = plt.subplots(1, 1)

        for Fint_i in Fint[:1000]:
            plt.plot(self.lna, Fint_i, c='r', ls='--', label="Original fit" if np.all(Fint_i == Fint[0]) else "")
        for Fint_i in Fint_red[:1000]:
            plt.plot(self.lna, Fint_i, c='b', ls=':', alpha=0.5,
                     label="Sampled in diagonal basis" if np.all(Fint_i ==
                                                                 Fint_red[0])
                     else "")

        plt.ylabel(self.fitted_function_label)
        plt.xlabel(self.time_label)
        plt.legend(loc=0)
        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling-fitted-function')

    def plot_w_evolution_original_and_sampled(self, n=1000):
        if not len(self.w_sampled):
            self.compute_w_original_and_sampled()

        w = self.w
        w_sampled = self.w_sampled
        Xw_sampled = Xw = self.lna

        f, ax = plt.subplots(1, 1)

        for wi, wj in zip(w[:n], w_sampled[:n]):

            plt.plot(Xw, wi, '-.', c='r', label='Original fit' if np.all(wi == w[0]) \
                                                            else "")
            plt.plot(Xw_sampled, wj, ':', c='b', label='Sampled in diagonal basis' \
                    if np.all(wj == w_sampled[0]) else "", alpha=0.5)

        plt.plot(Xw, -np.ones(len(Xw)), label='-1', c='black')

        plt.ylabel('w')
        plt.xlabel(self.time_label)
        plt.legend(loc=0)
        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling-w')

    def plot_w0wa_contours_original_and_sampled(self, num_contours=2, alpha=0.4,
                                                smooth_scale_2D=-1, ranges=True):

        w = self.w
        w_sampled = self.w_sampled
        lna = self.lna

        ################
        # Plot w0-wa comparison between original and sampled
        ################
        w0 = w[:, -1]
        w0_sampled = w_sampled[:, -1]
        wa = -(w0 - w[:, -2])/np.diff(np.exp(-lna[-2:]))
        wa_sampled = -(w0_sampled - w_sampled[:, -2])/np.diff(np.exp(-lna[-2:]))

        if ranges:
            ranges_w0 = (min(w0), None)
            ranges_w0_sampled = (min(w0_sampled), None)
            ranges_wa = (None, max(wa))
            ranges_wa_sampled = (None, max(wa_sampled))
        else:
            ranges_w0 = (None, None)
            ranges_w0_sampled = (None, None)
            ranges_wa = (None, None)
            ranges_wa_sampled = (None, None)

        names = ['w_0', 'w_a']
        labels = [r"w_0", r"w_a"]
        samples = GD.MCSamples(samples=[w0, wa], names=names, labels=labels,
                               label='Original fit', ranges={'w_0': ranges_w0, 'w_a': ranges_wa})
        samples2 = GD.MCSamples(samples=[w0_sampled, wa_sampled], names=names, labels=labels,
                                label='Sampled in diagonal basis',
                                ranges={'w_0': ranges_w0_sampled, 'w_a': ranges_wa_sampled})

        # Filled 2D comparison plot with legend
        g = plots.getSinglePlotter(width_inch=4, ratio=1)
        g.settings.alpha_filled_add = alpha
        g.settings.smooth_scale_2D = smooth_scale_2D
        g.settings.num_plot_contours = num_contours
        g.plot_2d([samples, samples2], 'w_0', 'w_a', filled=True)

        # xplot = np.linspace(-1.1, -0.7, 10)
        # plt.plot(xplot, -1.6*(1+xplot), '-.',  c='black', label='Thawing')
        plt.grid(True)
        g.add_legend(['Original fit', 'Sampled in diagonal basis'], legend_loc='lower right')
        self._close_fig(plt, g.fig, '-check-dimensional-reduction-from-sampling-w0wa')

    def plot_w0wa_fit_histograms(self, zmax=2.):
        if not len(self.hist_w0wa_fit_sampled.data):
            self.compute_w0wa_fit_original_and_sampled(zmax)

        f, ax = plt.subplots(1, 2, figsize=(7, 4))

        self._plot_histograms_comparison(ax, [self.hist_w0wa_fit, self.hist_w0wa_fit_sampled])

        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling-w0wa-fitted-histograms')

    def plot_w0wa_fit_contours_original_and_sampled(self, num_contours=2, alpha=0.4,
                                                smooth_scale_2D=-1, ranges=True, zmax=2.):
        if not len(self.hist_w0wa_fit_sampled.data):
            self.compute_w0wa_fit_original_and_sampled(zmax)


        w0, wa = co.remove_nans(self.hist_w0wa_fit.data)
        w0_sampled, wa_sampled = co.remove_nans(self.hist_w0wa_fit_sampled.data)

        lmodels = len(self.hist_w0wa_fit.data[0])
        removed = float(lmodels - len(w0) ) / lmodels

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of original models).\n'.format(removed))

        lmodels = len(self.hist_w0wa_fit_sampled.data[0])
        removed = float(lmodels - len(w0_sampled) ) / lmodels

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of sampled models).\n'.format(removed))

        ranges_w0 = (None, None)
        ranges_w0_sampled = (None, None)
        ranges_wa = (None, None)
        ranges_wa_sampled = (None, None)

        if not ranges:
            pass
        elif 'quintessence' in self.modelname:
            ranges_w0 = (min(w0), None)
            ranges_w0_sampled = (min(w0_sampled), None)
            ranges_wa = (None, max(wa))
            ranges_wa_sampled = (None, max(wa_sampled))

        names = ['w_0', 'w_a']
        labels = [r"w_0", r"w_a"]
        samples = GD.MCSamples(samples=[w0, wa], names=names, labels=labels,
                               label='Original fit', ranges={'w_0': ranges_w0, 'w_a': ranges_wa})
        samples2 = GD.MCSamples(samples=[w0_sampled, wa_sampled], names=names, labels=labels,
                                label='Sampled in diagonal basis',
                                ranges={'w_0': ranges_w0_sampled, 'w_a': ranges_wa_sampled})


        # Filled 2D comparison plot with legend
        g = plots.getSinglePlotter(width_inch=4, ratio=1)
        g.settings.alpha_filled_add = alpha
        g.settings.smooth_scale_2D = smooth_scale_2D
        g.settings.num_plot_contours = num_contours
        g.plot_2d([samples, samples2], 'w_0', 'w_a', filled=True)

        # xplot = np.linspace(-1.1, -0.7, 10)
        # plt.plot(xplot, -1.6*(1+xplot), '-.',  c='black', label='Thawing')
        g.fig.suptitle(r'Coeffs of $w \sim w_0 + w_a \log(a)$ fitted to {}'.format(self.fitted_function_label), fontsize='small')
        plt.grid(True)
        g.add_legend(['Original fit', 'Sampled in diagonal basis'], legend_loc='lower right')
        self._close_fig(plt, g.fig, '-check-dimensional-reduction-from-sampling-w0wa-fitted')

    def plot_H_DA_f_original_and_sampled(self, num_contours=2, alpha=0.4,
                                                   smooth_scale_2D=-1):
        if not len(self.obs_to_compare['H']):
            self.compute_H_DA_f_redshift_to_compare()
        if not len(self.obs_sampled_to_compare['H']):
            self.compute_H_DA_f_redshift_to_compare_sampled()

        removed = float(len(self.obs_to_compare['H'][0,
                                             np.isnan(self.obs_to_compare['H'][0])]))/len(self.obs_to_compare['H'][0])

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of original models).\n'.format(removed))

        removed = float(len(self.obs_sampled_to_compare['H'][0,
                                                     np.isnan(self.obs_sampled_to_compare['H'][0])]))/len(self.obs_to_compare['H'][0])

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of sampled models).\n'.format(removed))

        for i, z_bin in enumerate(self.redshift_to_compare):
            # Plot stuff
            ###############
            H, X, Y = self.obs_to_compare['H'][i], self.obs_to_compare['DA'][i], self.obs_to_compare['f'][i]
            H_sampled, X_red, Y_red = self.obs_sampled_to_compare['H'][i], self.obs_sampled_to_compare['DA'][i], self.obs_sampled_to_compare['f'][i]

            names = ['H', 'D_A', 'f']
            labels = [r"H", r"D_A", r"f"]
            samples = GD.MCSamples(samples=[H[~np.isnan(H)], X[~np.isnan(X)], Y[~np.isnan(Y)]], names=names, labels=labels,
                                   label='Original fit')
            samples2 = GD.MCSamples(samples=[H_sampled[~np.isnan(H_sampled)],
                                             X_red[~np.isnan(X_red)],
                                             Y_red[~np.isnan(Y_red)]],
                                    names=names, labels=labels, label='Sampled in diagonal basis')


            # Filled 2D comparison plot with legend
            g = plots.getSubplotPlotter()
            g.settings.legend_fontsize = 'small'
            g.settings.auto_ticks = True
            g.settings.alpha_filled_add = alpha
            g.settings.smooth_scale_2D = smooth_scale_2D
            g.settings.num_plot_contours = num_contours
            g.triangle_plot([samples, samples2], filled=True)

            # g = plots.getSinglePlotter(width_inch=7, ratio=1)
            # g.plot_2d([samples, samples2], 'f', 'D_A', filled=True)
            # plt.grid(True)
            # g.add_legend(['Original fit', 'Sampled in diagonal basis'], legend_loc='lower right')
            g.fig.suptitle('z = {}'.format(z_bin))
            self._close_fig(plt, g.fig, '-check-dimensional-reduction-from-sampling-H_DA_f-z-{}'.format(z_bin))

    def plot_DArec_original_and_sampled(self):

        if not len(self.obs_to_compare['DA_rec']):
            self.compute_H_DA_f_redshift_to_compare()

        if not len(self.obs_sampled_to_compare['DA_rec']):
            self.compute_H_DA_f_redshift_to_compare_sampled()

        DArec = co.remove_nans(self.obs_to_compare['DA_rec'])
        DArec_sampled = co.remove_nans(self.obs_sampled_to_compare['DA_rec'])

        lmodels = len(self.obs_to_compare['DA_rec'])
        removed = float(lmodels - len(DArec) ) / lmodels

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of original models).\n'.format(removed))

        lmodels = len(self.obs_sampled_to_compare['DA_rec'])
        removed = float(lmodels - len(DArec_sampled)) / lmodels

        if removed:
            sys.stdout.write('Removed failed computations ({:.2e}% of sampled models).\n'.format(removed))

        f, ax = plt.subplots(1, 1, figsize=(4,3))
        ax.hist([DArec, DArec_sampled], label=['Original', 'Sampled'],
                histtype='step', bins=self.nbins, density=True)

        self._close_fig(plt, f, '-check-dimensional-reduction-from-sampling-DArec')

