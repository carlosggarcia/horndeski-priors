#!/usr/bin/python2.7

from classy import Class

###############################

def save_dic(fname, dic):
    with open(fname, 'w') as f:
        for d in dic:
            f.write(str(d) + '\n')

###############################
paramfile = './quintessence_monomial-ints-params-analytical.txt-hiclass' # From quintessence_monomial-ints-fit-w-2-Taylor_legacy-(1-a)-36773

params = []
with open(paramfile, 'r') as f:
    for line in f:
        if '#' in line:
            continue
        pars = eval(line)
        pars['z_max_pk'] = 10
        pars['output'] = 'mPk'
        pars['parameters_smg'] = '1, -0.9999'
        del(pars['skip_stability_tests_smg'])
        del(pars['quintessence_safe_skip'])
        params.append(pars)


spectra_interp = []
pert_stepsize = []

spectra_other = []
pert_other = []

other = []

cosmo = Class()
errors = 0
for i, par in enumerate(params):
    cosmo.set(par)
    try:
        cosmo.compute()
    except Exception as e:
        msg = str(e)
        if 'spectra' in msg:
            if 'interpolate' in msg:
                spectra_interp.append(par)
            else:
                spectra_other.append(par)
        elif 'pert' in msg:
            if 'Step size' in msg:
                pert_stepsize.append(par)
            else:
                pert_other.append(par)
        else:
            other.append(par)
        errors += 1

    cosmo.struct_cleanup()
    cosmo.empty()

    if errors > 100:
        break

save_dic('quintessence_monomial-ints-params-spectra_interp.txt', spectra_interp)
save_dic('quintessence_monomial-ints-params-spectra_other.txt', spectra_other)
save_dic('quintessence_monomial-ints-params-pert_stepsize.txt', pert_stepsize)
save_dic('quintessence_monomial-ints-params-pert_other.txt', pert_other)
save_dic('quintessence_monomial-ints-params-other.txt', other)
