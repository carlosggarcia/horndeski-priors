#!/usr/bin/python

from glob import glob
import numpy as np
import os
import sys

class Data():
    def __init__(self, root):
        self.cosmo_arguments = {}
        self.parameters = {}
        self.varied_params = []
        self.root = root

        self.read_param_file()
        self.get_varied_params()

    def read_param_file(self):
        with open(os.path.join(self.root, 'log.param')) as f:
            for line in f:
                if 'data.path' in line:
                    continue
                if 'data.' == line[:5]:
                    exec('self.' + line[5:])

    def get_varied_params(self):
        self.varied_params = read_cosmo_params_varied(self.root)

    def get_current_value_params_cosmo_dic(self, chainstep):
        d = {}
        d.update(self.cosmo_arguments)
        for p, v in sorted(self.parameters.items()):
            if v[5] != 'cosmo':
                continue

            if v[3] == 0:
                d[p] = v[0]
            else:
                d[p] = chainstep[2 + self.varied_params.index(p)]

            if '__' not in p:
                continue

            pname, pindex = p.split('__')

            if pindex == '1':
                d[pname] = str(d[p])
            else:
                d[pname] += ", {}".format(d[p])

            del(d[p])

        lpar = len(self.varied_params)
        if chainstep.size > lpar + 2:  # Add the MP's parms
            p2smg = [str(i) for i in chainstep[lpar + 3:]]
            d['parameters_2_smg'] = ', '.join(p2smg)

        return d


# def read_cosmo_arguments(root):
#     data = Data()
#     with open(root + '/log.param') as f:
#         for line in f:
#             if 'data.' in line:
#                 exec(line)
#
#     return data.cosmo_arguments

def read_cosmo_params_varied(root):
    file = glob(os.path.join(root, "*.paramnames"))[0]
    params = []
    with open(file) as f:
        for line in f:
            params.append(line.split()[0])

    return params

def read_unequal_chains(file):
    lfile = []
    with open(file, 'r') as f:
        for line in f:
            lval = np.array([float(i) for i in line.split()])
            lfile.append(lval)

    return np.array(lfile)

def read_chains(root):
    files = sorted(glob(os.path.join(root, "*__*[0-9].txt")))
    chains = []
    for file in files:
        try:
            nchain = np.loadtxt(file)
        except ValueError:
            nchain = read_unequal_chains(file)

        chains.extend(nchain)

    return np.asarray(chains)

def hack_chains_params_2_smg(chains, lpars):
    parsmg = []
    par2smg = [[1, 0]]  # First step par2smg are not stored

    for c in chains:
        parsmg.append(c[:lpars + 2])
        par2smg.append(c[lpars + 2:])

    par2smg = par2smg[:-1]
    for i, p2smg in enumerate(par2smg):
        parsmg[i] = np.concatenate([parsmg[i], p2smg])

    return np.array(parsmg)

def create_chains_with_fitted_params(root_MP_chains, root_fit_chains):
    MP_chains = sorted(glob(os.path.join(root_MP_chains, "*__*[0-9].txt")))

    fit_chains = sorted(glob(os.path.join(root_fit_chains, "*__*[0-9]/*fit-w-0.txt")))
    fit_chains_number = sorted(glob(os.path.join(root_fit_chains, "*__*[0-9]/*numbers.txt")))

    fname = sorted(glob(os.path.join(root_fit_chains, "*__*[0-9]")))

    for MPcf, fcf, fcnf, fout in zip(MP_chains, fit_chains, fit_chains_number, fname):
        fcn = np.loadtxt(fcnf, unpack=True, dtype=int)

        MPc = np.loadtxt(MPcf, usecols=(0,1))[fcn-1]
        fc = np.loadtxt(fcf, ndmin=2)

        newchain = np.concatenate((MPc, fc), axis=1)
        np.savetxt(fout + '.txt', newchain)


if __name__ == "__main__":
    create_chains_with_fitted_params(sys.argv[1], sys.argv[2])

